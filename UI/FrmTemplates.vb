﻿Imports System.IO
Imports Microsoft.Office.Interop

Public Class FrmTemplates

    Private _Excel As clsExcelHelper


#Region " Estructura para almacenar las coordenadas de las celdas selecciondas "
    Private Structure SelectedOffsetStruct
        ReadOnly Property Row As Integer
        ReadOnly Property Col As Integer

        Public Sub New(Row As Integer, Col As Integer)
            _Row = Row
            _Col = Col
        End Sub
    End Structure
#End Region


#Region " Añadir y quitar archivos de la lista "
    Private Sub AddFilesToList(FileNames() As String)
        Dim FileList As List(Of String) = FileNames.ToList()
        Dim ErrorList As List(Of String) = New List(Of String)

        For Each Item As String In FileList
            Try
                AddFileToList(Item)
            Catch ex As ArgumentNullException
                ErrorList.Add($"{ex.ParamName} está vacío")
            Catch ex As ArgumentException
                ErrorList.Add(ex.Message)
            End Try
        Next

        If ErrorList.Count > 0 Then
            MessageBox.Show($"Errores al añadir a la lista: {vbCrLf}    {String.Join($"{vbCrLf}    ", ErrorList)}.")
        End If

    End Sub

    Private Sub AddFileToList(FullFilePath As String)
        If String.IsNullOrEmpty(FullFilePath) Then
            Throw New ArgumentNullException("FullFilePath")
        End If

        If Not My.Computer.FileSystem.FileExists(FullFilePath) Then
            Throw New ArgumentException($"El fichero {FullFilePath} no se encuentra.")
        End If

        Dim FileExtension As String = Path.GetExtension(FullFilePath).ToLower(Globalization.CultureInfo.InvariantCulture)
        If Not (FileExtension.Equals(".xlsx") Or FileExtension.Equals(".xls")) Then
            Throw New ArgumentException($"{FullFilePath} no es un fichero de Excel.")
        End If

        If LstFiles.Items.Contains(FullFilePath) Then
            Throw New ArgumentException($"{FullFilePath} ya está en la lista.")
        End If

        LstFiles.Items.Add(FullFilePath)
    End Sub

    Private Sub FrmTemplates_DragEnter(sender As Object, e As DragEventArgs) Handles Me.DragEnter
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            e.Effect = DragDropEffects.Copy
        End If
    End Sub

    Private Sub FrmTemplates_DragDrop(sender As Object, e As DragEventArgs) Handles Me.DragDrop
        AddFilesToList(e.Data.GetData(DataFormats.FileDrop))
    End Sub

    Private Sub BtnAddFile_Click(sender As Object, e As EventArgs) Handles BtnAddFile.Click
        With OpenFileDialog1
            .CheckFileExists = True
            .Filter = "Microsoft Excel Files|*.xlsx;*.xls"
            .FileName = ""
            .Multiselect = True
            .ValidateNames = True
            If .ShowDialog() = DialogResult.OK Then
                AddFilesToList(.FileNames)
            End If
        End With
    End Sub

    Private Sub LstFiles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstFiles.SelectedIndexChanged
        Dim Enabled As Boolean = (LstFiles.SelectedItems.Count > 0)

        BtnRemoveFile.Enabled = Enabled
        BtnPreview.Enabled = Enabled
        BtnShowReport.Enabled = Enabled
    End Sub

    Private Sub BtnRemoveFile_Click(sender As Object, e As EventArgs) Handles BtnRemoveFile.Click
        If LstFiles.SelectedItems.Count.Equals(0) Then Return
        _Excel.RemoveWorkBook(LstFiles.Items(LstFiles.SelectedIndex))
        LstFiles.Items.RemoveAt(LstFiles.SelectedIndex)
    End Sub
#End Region


#Region " Vista Previa del archivo "
    Private Sub BtnPreview_Click(sender As Object, e As EventArgs) Handles BtnPreview.Click
        Dim CurrentRow As Integer = 0
        Dim CurrentCol As Integer = 0

        If LstFiles.SelectedItems.Count.Equals(0) Then Return

        BtnPreview.Enabled = False
        Cursor = Cursors.WaitCursor

        GrdPreview.Rows.Clear()
        GrdPreview.Columns.Clear()

        Dim SelectedFile As String = LstFiles.Items(LstFiles.SelectedIndex)

        Try
            If _Excel Is Nothing Then _Excel = New clsExcelHelper

            Dim Ws As Excel.Worksheet = _Excel.GetSheet(SelectedFile, 1)
            Dim FirstCell As Excel.Range
            Dim LastCell As Excel.Range
            Dim ColCount As Integer

            With Ws.UsedRange
                FirstCell = .Cells(1, 1)
                LastCell = .Cells(.Rows.Count + FirstCell.Row, .Columns.Count + FirstCell.Column)
                FirstCell = Ws.Range("A1")
            End With

            ColCount = LastCell.Column - FirstCell.Column + 1

            For i As Integer = 0 To ColCount - 1
                GrdPreview.Columns.Add(i.ToString, i.ToString)
            Next

            For row As Integer = FirstCell.Row To LastCell.Row
                Dim RowArray(ColCount - 1) As String
                CurrentCol = 0
                For col As Integer = FirstCell.Column To LastCell.Column
                    RowArray(CurrentCol) = FirstCell.Offset(CurrentRow, CurrentCol).Value
                    CurrentCol += 1
                Next
                GrdPreview.Rows.Add(RowArray)
                CurrentRow += 1
            Next

            GrdPreview.AutoResizeColumns()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        BtnPreview.Enabled = True
        Cursor = Cursors.Default
    End Sub

    Private Sub FrmTemplates_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If _Excel IsNot Nothing Then _Excel.Dispose()
    End Sub

#End Region


#Region " Informe "
    Private Sub BtnShowReport_Click(sender As Object, e As EventArgs) Handles BtnShowReport.Click
        If LstFiles.Items.Count.Equals(0) Then Return

        Dim SelectedOffsets As List(Of SelectedOffsetStruct) = GetSelectedOffsets()
        If SelectedOffsets.Count.Equals(0) Then Return

        Try
            CheckSelectedOffsets(SelectedOffsets)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return
        End Try

        ' Ordenamos los rangos
        SelectedOffsets.Sort(Function(x, y) x.Row.CompareTo(y.Row))
        SelectedOffsets.Sort(Function(x, y) x.Col.CompareTo(y.Col))

        BtnShowReport.Enabled = False
        Cursor = Cursors.WaitCursor

        ShowReport(SelectedOffsets)

        BtnShowReport.Enabled = True
        Cursor = Cursors.Default
    End Sub

    Private Function GetSelectedOffsets() As List(Of SelectedOffsetStruct)
        Dim SelectedOffsets As List(Of SelectedOffsetStruct) = New List(Of SelectedOffsetStruct)

        For Each cell As DataGridViewCell In GrdPreview.SelectedCells
            SelectedOffsets.Add(New SelectedOffsetStruct(cell.RowIndex, cell.ColumnIndex))
        Next

        Return SelectedOffsets
    End Function

    Private Sub CheckSelectedOffsets(SelectedOffsets As List(Of SelectedOffsetStruct))
        Dim RowCount = GetRowCount(SelectedOffsets)
        Dim ColCount = GetColCount(SelectedOffsets)

        If RowCount.Equals(0) Or ColCount.Equals(0) Then
            Throw New Exception("No hay celdas seleccionadas.")
        End If

        If RowCount > 1 And ColCount > 1 Then
            Throw New Exception($"Se esperaban celdas de una fila o de una columna. Se han recibido {RowCount} filas y {ColCount} columnas.")
        End If
    End Sub

    Private Function GetColCount(SelectedOffsets As List(Of SelectedOffsetStruct)) As Integer
        Dim ColList As New List(Of Integer)

        For Each Offset As SelectedOffsetStruct In SelectedOffsets
            If Not ColList.Contains(Offset.Col) Then
                ColList.Add(Offset.Col)
            End If
        Next

        Return ColList.Count
    End Function

    Private Function GetRowCount(SelectedOffsets As List(Of SelectedOffsetStruct)) As Integer
        Dim RowList As New List(Of Integer)

        For Each Offset As SelectedOffsetStruct In SelectedOffsets
            If Not RowList.Contains(Offset.Row) Then
                RowList.Add(Offset.Row)
            End If
        Next

        Return RowList.Count
    End Function

    Private Sub ShowReport(SelectedOffsets As List(Of SelectedOffsetStruct))
        Dim ExcelReport As clsExcelHelper = New clsExcelHelper
        Dim Wb As Excel.Workbook = ExcelReport.GetNewWorkbook()
        Dim WsTraspuesta As Excel.Worksheet = _Excel.GetNewSheet(Wb, "Traspuesta")
        Dim RangeTraspuesta As Excel.Range = WsTraspuesta.Range("A1")
        Dim WsNormal As Excel.Worksheet = _Excel.GetNewSheet(Wb, "Normal")
        Dim RangeNormal As Excel.Range = WsNormal.Range("A1")
        Dim ColCount As Integer = GetColCount(SelectedOffsets)
        Dim CurrentColOffset As Integer
        Dim CurrentRowOffset As Integer

        For Each fileItem As String In LstFiles.Items
            Dim Ws As Excel.Worksheet = _Excel.GetSheet(fileItem, 1)

            If ColCount.Equals(1) Then
                CurrentRowOffset += 1
                CurrentColOffset = 0
            Else
                CurrentColOffset += 1
                CurrentRowOffset = 0
            End If

            For Each offsetItem As SelectedOffsetStruct In SelectedOffsets
                If ColCount.Equals(1) Then
                    CurrentColOffset += 1
                Else
                    CurrentRowOffset += 1
                End If
                RangeNormal.Offset(CurrentColOffset, CurrentRowOffset).Value = Ws.Range("A1").Offset(offsetItem.Row, offsetItem.Col).Value
                RangeTraspuesta.Offset(CurrentRowOffset, CurrentColOffset).Value = Ws.Range("A1").Offset(offsetItem.Row, offsetItem.Col).Value
            Next
        Next

        ExcelReport.Show(Wb, New List(Of String) From {
                         "Traspuesta",
                         "Normal"}
                         )
    End Sub
#End Region

End Class