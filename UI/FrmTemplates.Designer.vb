﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmTemplates
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LstFiles = New System.Windows.Forms.ListBox()
        Me.BtnAddFile = New System.Windows.Forms.Button()
        Me.BtnRemoveFile = New System.Windows.Forms.Button()
        Me.GrdPreview = New System.Windows.Forms.DataGridView()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.BtnPreview = New System.Windows.Forms.Button()
        Me.BtnShowReport = New System.Windows.Forms.Button()
        CType(Me.GrdPreview, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LstFiles
        '
        Me.LstFiles.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LstFiles.FormattingEnabled = True
        Me.LstFiles.Location = New System.Drawing.Point(13, 13)
        Me.LstFiles.Name = "LstFiles"
        Me.LstFiles.Size = New System.Drawing.Size(588, 147)
        Me.LstFiles.TabIndex = 0
        '
        'BtnAddFile
        '
        Me.BtnAddFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnAddFile.Location = New System.Drawing.Point(607, 13)
        Me.BtnAddFile.Name = "BtnAddFile"
        Me.BtnAddFile.Size = New System.Drawing.Size(75, 23)
        Me.BtnAddFile.TabIndex = 1
        Me.BtnAddFile.Text = "Añadir"
        Me.BtnAddFile.UseVisualStyleBackColor = True
        '
        'BtnRemoveFile
        '
        Me.BtnRemoveFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnRemoveFile.Enabled = False
        Me.BtnRemoveFile.Location = New System.Drawing.Point(607, 42)
        Me.BtnRemoveFile.Name = "BtnRemoveFile"
        Me.BtnRemoveFile.Size = New System.Drawing.Size(75, 23)
        Me.BtnRemoveFile.TabIndex = 2
        Me.BtnRemoveFile.Text = "Quitar"
        Me.BtnRemoveFile.UseVisualStyleBackColor = True
        '
        'GrdPreview
        '
        Me.GrdPreview.AllowUserToAddRows = False
        Me.GrdPreview.AllowUserToDeleteRows = False
        Me.GrdPreview.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdPreview.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCellsExceptHeader
        Me.GrdPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdPreview.ColumnHeadersVisible = False
        Me.GrdPreview.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.GrdPreview.Location = New System.Drawing.Point(13, 166)
        Me.GrdPreview.Name = "GrdPreview"
        Me.GrdPreview.RowHeadersVisible = False
        Me.GrdPreview.Size = New System.Drawing.Size(670, 385)
        Me.GrdPreview.TabIndex = 5
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'BtnPreview
        '
        Me.BtnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPreview.Enabled = False
        Me.BtnPreview.Location = New System.Drawing.Point(608, 72)
        Me.BtnPreview.Name = "BtnPreview"
        Me.BtnPreview.Size = New System.Drawing.Size(75, 23)
        Me.BtnPreview.TabIndex = 3
        Me.BtnPreview.Text = "Preview"
        Me.BtnPreview.UseVisualStyleBackColor = True
        '
        'BtnShowReport
        '
        Me.BtnShowReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnShowReport.Enabled = False
        Me.BtnShowReport.Location = New System.Drawing.Point(607, 101)
        Me.BtnShowReport.Name = "BtnShowReport"
        Me.BtnShowReport.Size = New System.Drawing.Size(75, 23)
        Me.BtnShowReport.TabIndex = 4
        Me.BtnShowReport.Text = "Report"
        Me.BtnShowReport.UseVisualStyleBackColor = True
        '
        'FrmTemplates
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(694, 563)
        Me.Controls.Add(Me.BtnShowReport)
        Me.Controls.Add(Me.BtnPreview)
        Me.Controls.Add(Me.GrdPreview)
        Me.Controls.Add(Me.BtnRemoveFile)
        Me.Controls.Add(Me.BtnAddFile)
        Me.Controls.Add(Me.LstFiles)
        Me.Name = "FrmTemplates"
        Me.Text = "Templates"
        CType(Me.GrdPreview, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LstFiles As ListBox
    Friend WithEvents BtnAddFile As Button
    Friend WithEvents BtnRemoveFile As Button
    Friend WithEvents GrdPreview As DataGridView
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents BtnPreview As Button
    Friend WithEvents BtnShowReport As Button
End Class
