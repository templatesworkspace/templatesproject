﻿Imports Microsoft.Office.Interop

''' <summary>
''' Se encargar de realizar operaciones básicas con Documentos de Excel
''' </summary>
Public Class clsExcelHelper
    Implements IDisposable
    Private _Excel As Excel.Application
    ''' <summary>
    ''' Almacena en memoria los libros de Excel
    ''' </summary>
    Private _WorkbooksDict As Dictionary(Of String, Excel.Workbook)

    Sub New()
        _Excel = New Excel.Application
        _WorkbooksDict = New Dictionary(Of String, Excel.Workbook)
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        _WorkbooksDict = Nothing
        _Excel.DisplayAlerts = False
        For Each Wb As Excel.Workbook In _Excel.Workbooks
            Wb.Close(0)
        Next
        _Excel.Quit()
        _Excel = Nothing
    End Sub

    ''' <summary>
    ''' Añade al diccionario de libros el libro 'FilePath'
    ''' </summary>
    ''' <param name="FilePath"></param>
    ''' <remarks>Si el libro ya existe en el diccionario no se añade ni se actualiza la referencia</remarks>
    ''' <exception cref="ArgumentException">El fichero no existe</exception>
    Private Sub AddWorkBook(FilePath As String)
        If Not My.Computer.FileSystem.FileExists(FilePath) Then
            Throw New ArgumentException($"El fichero {FilePath} no existe")
        End If

        If _WorkbooksDict.ContainsKey(FilePath) Then Return

        _Excel.EnableEvents = False
        _WorkbooksDict.Add(FilePath, _Excel.Workbooks.Open(FilePath))
        _Excel.EnableEvents = True
    End Sub

    ''' <summary>
    ''' Elimina del diccionario de libros el libro 'FilePath'
    ''' </summary>
    ''' <param name="FilePath"></param>
    ''' <remarks>Si el libro no existe en el diccionario no se hace nada</remarks>
    Public Sub RemoveWorkBook(FilePath As String)
        If _WorkbooksDict.ContainsKey(FilePath) Then
            _WorkbooksDict.Remove(FilePath)
        End If
    End Sub

    ''' <summary>
    ''' Obtiene una referencia a la hoja con indice 'SheetIndex' del fivhero 'FileName'
    ''' </summary>
    ''' <param name="FileName"></param>
    ''' <param name="SheetIndex"></param>
    ''' <returns>Referencia a la hoja de Excel (Excel.Worksheet)</returns>
    ''' <exception cref="IndexOutOfRangeException">No existe ninguna hoja con el índice 'SheetIndex'</exception>
    Public Function GetSheet(FileName As String, SheetIndex As Integer) As Excel.Worksheet
        If Not _WorkbooksDict.ContainsKey(FileName) Then
            AddWorkBook(FileName)
        End If

        If SheetIndex < 1 Or SheetIndex > _WorkbooksDict(FileName).Sheets.Count Then
            Throw New IndexOutOfRangeException("La hoja especificada no existe.")
        End If

        Return _WorkbooksDict(FileName).Worksheets(SheetIndex)
    End Function

    ''' <summary>
    ''' Crea un nuevo libro de Excel
    ''' </summary>
    ''' <returns>Referencia al nuevo libro de Excel (Excel.Workbook)</returns>
    Public Function GetNewWorkbook() As Excel.Workbook
        Return _Excel.Workbooks.Add
    End Function

    ''' <summary>
    ''' Crea una nueva hoja de Excel y se le asigna el nombre 'WorksheetName'
    ''' </summary>
    ''' <param name="Wb"></param>
    ''' <param name="WorksheetName"></param>
    ''' <returns>Referencia a la nueva hoja de Excel (Excel.Worksheet)</returns>
    Public Function GetNewSheet(Wb As Excel.Workbook, WorksheetName As String) As Excel.Worksheet
        For Each WsItem As Excel.Worksheet In Wb.Worksheets
            If WsItem.Name.Equals(WorksheetName, StringComparison.InvariantCultureIgnoreCase) Then
                Return WsItem
            End If
        Next

        Dim Ws As Excel.Worksheet = Wb.Worksheets.Add()
        Ws.Name = WorksheetName
        Return Ws
    End Function

    ''' <summary>
    ''' Muestra Excel
    ''' </summary>
    Public Sub Show()
        ' Muestra Excel
        _Excel.Visible = True
    End Sub

    ''' <summary>
    ''' Itera las Hojas del libro 'Wb' y elimina aquellas que no estén en la lista 'SheetNames'
    ''' </summary>
    ''' <param name="Wb"></param>
    ''' <param name="SheetNames"></param>
    Public Sub Show(Wb As Excel.Workbook, SheetNames As List(Of String))
        For Each WsItem As Excel.Worksheet In Wb.Worksheets
            Dim ExistsSheet As Boolean = False
            For Each SheetName As String In SheetNames
                If WsItem.Name.Equals(SheetName, StringComparison.InvariantCultureIgnoreCase) Then
                    ExistsSheet = True
                    Exit For
                End If
            Next
            If Not ExistsSheet Then
                _Excel.DisplayAlerts = False
                WsItem.Delete()
                _Excel.DisplayAlerts = True
            End If
        Next
        Show()
    End Sub
End Class